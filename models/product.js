const mongoose = require('mongoose');
//Create Schema and model

//Schema

const productSchema = new mongoose.Schema({
	name: {
		type: String,
		required: true,
		max: 100
	},
	price: {
		type: Number,
		required: true
	}
});

//Model

module.exports = mongoose.model("Product",productSchema);