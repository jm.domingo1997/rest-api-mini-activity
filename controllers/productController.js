//Model
const Product = require('../models/product');

//Get all products
module.exports.getAllProducts = () => {
	return Product.find({})
	.then(result =>{ return result});
}

//Add a new product
module.exports.addProduct = (requestBody) => {


return Product.findOne({name:requestBody.name}).then((result,err)=>{
	if(result != null && result.name == requestBody.name){
		return `Duplicate was found`;
	}else{
		let newProduct = new Product({
			name: requestBody.name,
			price: requestBody.price
		});
	
		return newProduct.save().then((product,error) => {
			if(error){
				console.log(error)
			}else{
				return product;
			}
		})
	}
})
	
}

//Get single product
module.exports.getProduct = (productId) => {
	return Product.findById(productId).then((result,err)=>{
		if(err){
			console.log(err)
			return false;
		}else{
			return result;
		}
	})
}

//Delete Product
module.exports.deleteProduct = (productId) => {
	return Product.findByIdAndRemove(productId).then((removedProduct,err)=>{
		if(err){
			console.log(err);
			return false;
		}else{
			return removedProduct;
		}
	})
}
