const express = require('express');
const mongoose = require('mongoose');

//This allows the apps to use all the routes defined in the productRoute.js
const productRoute = require('./routes/productRoute')

const app = express();
const port = 3002;

app.use(express.json());
app.use(express.urlencoded({extended:true}))

//Connect to MongoDB
mongoose.connect('mongodb+srv://admin:admin1234@zuitt-bootcamp.eqilw.mongodb.net/RestAPIMiniActivity?retryWrites=true&w=majority',
	{useNewUrlParser:true,useUnifiedTopology:true});

//to use "/products" route
app.use('/products',productRoute);

//Server listening
app.listen(port,()=>{
	console.log(`Listening to port ${port}`)
});
