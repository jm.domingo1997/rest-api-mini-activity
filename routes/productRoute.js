const express = require('express');
const router = express.Router();

const productController = require('../controllers/productController');

//Route for getting all products
router.get('/',(req,res)=>{
	productController.getAllProducts()
	.then(resultFromController => res.send(resultFromController));
});

//Route to add a new product
router.post('/create',(req,res)=>{
	 productController.addProduct(req.body)
	 .then(resultFromController => res.send(resultFromController));
})

//Route for getting a specific id
router.get('/:id',(req,res)=>{
	productController.getProduct(req.params.id)
	.then(resultFromController => res.send(resultFromController));
})

//Route to delete a product
router.delete('/delete/:id',(req,res)=>{
	productController.deleteProduct(req.params.id)
	.then(resultFromController => res.send(resultFromController));
})



//Use module.exports to export the router object to use in the "app"
module.exports = router;